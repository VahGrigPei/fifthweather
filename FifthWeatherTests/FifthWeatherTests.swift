//
//  FifthWeatherTests.swift
//  FifthWeatherTests
//
//  Created by Vahan Grigoryan on 9/10/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import XCTest
@testable import FifthWeather

class FifthWeatherTests: XCTestCase {
    

    func testSearchFunction() {
        let homeController = CountryListController()
        
        self.getCapitals { (capitals, error) in
            if error == nil {
                XCTAssertEqual(capitals?.count, 250)
                AppManager.shared.capitals = capitals
                homeController.citiesFor(searchText: "b")
                XCTAssertEqual(homeController.searchedCapitals?.count, 57)
                homeController.citiesFor(searchText: "Libreville")
                XCTAssertEqual(homeController.searchedCapitals?.first?.capital, "Libreville", "Bad json")
            } else {
                XCTFail(error?.localizedDescription ?? "error when parsing")
            }
        }
    }

    func getCapitals(completion: @escaping ([City]?, Error?) -> Void) {
        
        let currentBundle = Bundle(for: type(of: self))
        if let path = currentBundle.path(forResource: "Capitals", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                let capitals = try? decoder.decode([City].self, from: data)
                completion(capitals, nil)
            } catch {
                completion(nil,error)
            }
        }
    }
}
