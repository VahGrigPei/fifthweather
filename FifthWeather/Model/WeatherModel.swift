//
//  WeatherModel.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

struct WeatherModel: Codable {
    var name: String?
    var weather: [Weather]?
    var main: MainData?
    var wind: Wind?
    var sys: Sys?
}

extension WeatherModel {
    
    func weatherIconUrl() -> URL? {
        var urlString: String?
        let baseIconString = "https://openweathermap.org/img/wn/"
        
        guard let icon = self.weather?.first?.icon else {return nil}
        urlString = baseIconString + icon + "@2x.png"
        return URL(string: urlString ?? "")
    }
    
    func temperature() -> String {
        return String(Int(self.main?.temp! ?? 0)) + "°"
    }
    
    func maximumTemperature() -> String {
        return String(self.main?.temp_max ?? 0) + "°"
    }
    
    func minimumTemperature() -> String {
        return String(self.main?.temp_min ?? 0) + "°"
    }
    
    func pressurehPa() -> String {
        return String(Int(self.main?.pressure ?? 0)) + " hPa"
    }
    
    func windSpeed() -> String {
        return String(self.wind?.speed ?? 0) + " m/s"
    }
    
    func humidityPressent() -> String {
        return String(Int(self.main?.humidity ?? 0)) + " %"
    }
    
    func sunrise() -> String {
        return String(Date().secondsToTime(time: self.sys?.sunrise ?? 0))
    }
    
    func sunset() -> String {
        return String(Date().secondsToTime(time: self.sys?.sunset ?? 0))
    }
    
}
