//
//  Capital.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

struct City: Codable {
    var capital: String?
    var isSelected: Bool?
}
