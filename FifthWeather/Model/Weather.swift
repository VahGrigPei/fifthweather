//
//  Weather.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

struct Weather: Codable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
}
