//
//  APIClient+City.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import Alamofire

extension APIClient {
    
    func getWeatherDataFor(city: String, completion: @escaping (WeatherModel?, Error?) -> Void) {
        let countryAllowed = city.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        let parametrs = ["q" : countryAllowed]
        Alamofire.request(baseUrl, method: .get, parameters: parametrs).responseJSON { (response) in
            
            if response.result.isSuccess, let data = response.data {
                self.parseResponse(data: data, completion: { (model, error) in
                    completion(model, error)
                })
            } else {
                completion(nil, response.error)
            }
        }
    }
    
    func getCities(completion: @escaping ([City]?, Error?) -> Void) {
        guard let capitalsUrl = URL(string: "https://restcountries.eu/rest/v2/all?fields=capital") else {
            return
        }
        Alamofire.request(capitalsUrl, method: .get, parameters: nil).responseJSON { (response) in
            if response.result.isSuccess, let data = response.data {
                self.parseResponse(data: data, completion: { (model, error) in
                    completion(model, error)
                })
            } else {
                completion(nil, response.error)
            }
        }
    }
}
