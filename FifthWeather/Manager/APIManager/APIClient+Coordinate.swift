//
//  APIClient+Coordinate.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import Alamofire

extension APIClient {
    
    func getWeatherDataFor(latitude: String, longitude: String, completion: @escaping (WeatherModel?, Error?) -> Void) {
        let parametrs = ["lat" : latitude, "lon" : longitude]
        Alamofire.request(baseUrl, method: .get, parameters: parametrs).responseJSON { (response) in
            
            if response.result.isSuccess, let data = response.data {
                self.parseResponse(data: data, completion: { (model, error) in
                    completion(model, error)
                })
            } else {
                completion(nil, response.error)
            }
        }
    }
}
