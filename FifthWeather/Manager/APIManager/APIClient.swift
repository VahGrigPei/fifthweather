//
//  APIClient.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

class APIClient {
    
    static let shared = APIClient()
    private let appId = "2792b0f190303589d990b08518cad6ec"
    var baseUrl: String! {
        get {
            return "https://api.openweathermap.org/data/2.5/weather?units=metric&appid=" + appId
        }
    }
    
    func parseResponse<T: Decodable>(data: Data, completion: (T?, Error?) -> Void) {
        
        let error = try? JSONDecoder().decode(APIError.self, from: data)
        if error?.message != nil {
            completion(nil, error)
        } else {
            do {
                let model = try JSONDecoder().decode(T.self, from: data)
                completion(model, nil)
            } catch let jsonErr {
                completion(nil, jsonErr)
            }
        }
    }
}
