//
//  APIError.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

struct APIError: LocalizedError, Codable {
    
    let code: Int?
    let message: String?
    let error: String?
    
    var errorDescription: String? { return message }
    var localizedDescription: String? { return message }
    
}
