//
//  SecondViewController.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit

class CountryListViewController: UIViewController {
    
    @IBOutlet weak var capitalstableView: UITableView!
    
    private let viewModel = CountryListViewModel()
    private let keyboardHeight: CGFloat = 224
    private let cityWeatherCell = "CityWeatherCell"
    private let cityCell = "CityCell"
    private var lastExpandedRow: Int?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initiateUI()
    }
    
    override func viewDidLayoutSubviews() {
        self.capitalstableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
    }
    
    func initiateUI() {
        self.capitalstableView.keyboardDismissMode = .onDrag
        viewModel.getCapitals()
        viewModel.onCapitalsSet = {
            self.lastExpandedRow = 0
            self.capitalstableView.reloadData()
        }

        viewModel.onWeatherSet = { isWeatherSet in
            if isWeatherSet {
                self.updateRowInTableView()
            }
        }
    }
    
    func updateRowInTableView() {
        self.capitalstableView.beginUpdates()
        self.capitalstableView.reloadRows(at: [IndexPath(item: self.lastExpandedRow ?? 0, section: 0)], with: .automatic)
        self.capitalstableView.endUpdates()
    }
    
}

extension CountryListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.searchedCapitals?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let city = viewModel.searchedCapitals?[indexPath.row] else { return UITableViewCell() }
        if city.isSelected ?? false {
            let cell = tableView.dequeueReusableCell(withIdentifier: cityWeatherCell) as! CityWeatherCell
            cell.weather = viewModel.watherForCity
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell") as! CityCell
            cell.capitalLabel.text = city.capital
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) as? CityCell {
            viewModel.searchedCapitals?[self.lastExpandedRow ?? 0].isSelected = false
            self.updateRowInTableView()
            let capital = cell.capitalLabel.text
            viewModel.searchedCapitals?[indexPath.row].isSelected = true
            viewModel.getWeatherFor(city: capital ?? "")
        } else if ((tableView.cellForRow(at: indexPath) as? CityWeatherCell) != nil) {
            viewModel.searchedCapitals?[self.lastExpandedRow ?? 0].isSelected = false
            self.updateRowInTableView()
        }
        self.lastExpandedRow = indexPath.row
    }
    
}

extension CountryListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.citiesFor(searchText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        viewModel.citiesFor(searchText: "")
        self.view.endEditing(true)
    }
    
    
}
