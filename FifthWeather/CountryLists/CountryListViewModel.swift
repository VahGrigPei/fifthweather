//
//  File.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

class CountryListViewModel {
    
    var onCapitalsSet: (() -> Void)?
    var onWeatherSet: ((Bool) -> Void)?
    var watherForCity: WeatherModel?
    
    var searchedCapitals: [City]?
    
    func getCapitals() {
        guard AppManager.shared.capitals == nil else {return}
        APIClient.shared.getCities { (capitals, error) in
            if let error = error {
                MessageAlert().weatherErrorWith(title: "", body: error.localizedDescription)
            } else if let capitals = capitals?.filter({$0.capital != ""}) {
                AppManager.shared.capitals = capitals
                self.searchedCapitals = capitals
                self.onCapitalsSet?()
            }
        }
    }
    
    func getWeatherFor(city: String) {
        APIClient.shared.getWeatherDataFor(city: city) { (weatherModel, error) in
            if let error = error {
                MessageAlert().weatherErrorWith(title: "", body: error.localizedDescription)
            } else if let weather = weatherModel {
                self.watherForCity = weather
                self.onWeatherSet?(self.watherForCity != nil)
            }
        }
    }
    
    func citiesFor(searchText: String) {
        if searchText.isEmpty {
            searchedCapitals = AppManager.shared.capitals
        } else {
            guard let cities = AppManager.shared.capitals else { return }
            let prefixArray = cities.filter {($0.capital?.hasPrefix(searchText) ?? false)}
            let containArray = cities.filter {((($0.capital?.containsIgnoringCase(find: searchText)) ?? false) && !prefixArray.contains(where: { $0.capital == $0.capital}))}
            searchedCapitals = prefixArray + containArray
        }
        onCapitalsSet?()
    }
}
