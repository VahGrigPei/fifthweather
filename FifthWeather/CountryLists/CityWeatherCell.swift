//
//  CityWeatherCell.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit
import SDWebImage

class CityWeatherCell: UITableViewCell {

    @IBOutlet weak var capitalLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var minTemperatureLabel: UILabel!
    @IBOutlet weak var maxTemperatureLabel: UILabel!
    
    var weather: WeatherModel? {
        didSet {
            capitalLabel.text = weather?.name
            temperatureLabel.text = weather?.temperature()
            if let url = weather?.weatherIconUrl() {
                weatherImageView.sd_setImage(with: url, completed: nil)
            }
            minTemperatureLabel.text = "Min Temp: " + (weather?.minimumTemperature() ?? "")
            maxTemperatureLabel.text = "Max Temp: " + (weather?.maximumTemperature() ?? "")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
