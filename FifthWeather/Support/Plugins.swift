//
//  Plugins.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/9/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import SwiftMessages

class MessageAlert: NSObject {
    
    func weatherErrorWith(title: String, body: String) {
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(.error)
        view.button?.isHidden = true
        view.titleLabel?.textColor = UIColor.black
        view.backgroundView.backgroundColor = UIColor.red
        view.configureDropShadow()
        view.configureContent(title: title, body: body, iconText: "🤔")
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 5
        SwiftMessages.show(view: view)
    }
    
}
