//
//  File.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/9/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

extension String {
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
