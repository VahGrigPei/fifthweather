//
//  FifthWeather+Date.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

extension Date {
    
    func secondsToTime(time: Int) -> String {
        let dateTime = Date(timeIntervalSince1970: (Double(time)))
        let apiTimeOnlyFormatter = DateFormatter()
        apiTimeOnlyFormatter.timeZone = TimeZone.current
        apiTimeOnlyFormatter.dateFormat = "HH:mm a"
        return apiTimeOnlyFormatter.string(from: dateTime)
    }
}
