//
//  HomeViewModel.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

class HomeViewModel {
    var onWeatherModelChange: (() -> Void)?
    var weatherModel: WeatherModel? {
        didSet {
            onWeatherModelChange?()
        }
    }
    
    var isFetching = false
    
    // MARK: Get Weather Data For latitude and longitude
    func getWeatherDataFor(latitude: String, longitude: String) {
        if isFetching { return }
        self.isFetching = true
        APIClient.shared.getWeatherDataFor(latitude: latitude, longitude: longitude) { [weak self] (model, error)  in
            self?.isFetching = false
            if error == nil {
                self?.weatherModel = model
            } else if let message = error?.localizedDescription {
                MessageAlert().weatherErrorWith(title: "", body: message)
            }
        }
    }
    
    // MARK: Get Weather Data For City
    func getWeatherDataFor(city: String) {
        if isFetching { return }
        self.isFetching = true
        APIClient.shared.getWeatherDataFor(city: city) { [weak self] (model, error)  in
            self?.isFetching = false
            if error == nil {
                self?.weatherModel = model
            } else if let message = error?.localizedDescription {
                MessageAlert().weatherErrorWith(title: "", body: message)
            }
        }
    }
    
}
