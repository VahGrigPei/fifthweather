//
//  FirstViewController.swift
//  FifthWeather
//
//  Created by Vahan Grigoryan on 9/8/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage

class HomeViewController: UIViewController {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunSetLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var HumidityLabel: UILabel!
    @IBOutlet weak var minTemperatureLabel: UILabel!
    @IBOutlet weak var maxTemperatureLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!

   private lazy var viewModel: HomeViewModel = {
        return  HomeViewModel()
    }()
    private let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialUI()
    }
    
    private func initialUI() {
        showLoading(state: true)
        locationConfiguration()
        backgroundImageView.makeBlur()
        viewModel.onWeatherModelChange = { [weak self] in
            self?.updateUI()
        }
    }
    
    private func updateUI() {
        showLoading(state: false)
        cityLabel.text = viewModel.weatherModel?.name
        self.temperatureLabel.text = viewModel.weatherModel?.temperature()
        if let url = self.viewModel.weatherModel?.weatherIconUrl() {
            weatherImageView.sd_setImage(with: url, completed: nil)
        }
        sunriseLabel.text = viewModel.weatherModel?.sunrise()
        sunSetLabel.text = viewModel.weatherModel?.sunset()
        windLabel.text = viewModel.weatherModel?.windSpeed()
        HumidityLabel.text = viewModel.weatherModel?.humidityPressent()
        maxTemperatureLabel.text = viewModel.weatherModel?.maximumTemperature()
        minTemperatureLabel.text = viewModel.weatherModel?.minimumTemperature()
    }
    
    private func showLoading(state: Bool) {
        if state {
            cityLabel.text = "Loading..."
            temperatureLabel.isHidden = true
            weatherImageView.isHidden = true
        } else {
            cityLabel.isHidden = false
            temperatureLabel.isHidden = false
            weatherImageView.isHidden = false
        }
    }
    
    private func locationConfiguration() {
        locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.startUpdatingLocation()
    }
}

extension HomeViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last, location.horizontalAccuracy > 0 {
            locationManager.stopUpdatingLocation()
            let longitude = String(location.coordinate.longitude)
            let latitude = String(location.coordinate.latitude)
            viewModel.getWeatherDataFor(latitude: latitude, longitude: longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        cityLabel.text = "Unvailable"
    }
    
}

